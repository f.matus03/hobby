import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BidderspanelComponent } from './bidderspanel.component';

describe('BidderspanelComponent', () => {
  let component: BidderspanelComponent;
  let fixture: ComponentFixture<BidderspanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BidderspanelComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(BidderspanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
