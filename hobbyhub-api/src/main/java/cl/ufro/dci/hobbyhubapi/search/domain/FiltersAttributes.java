package cl.ufro.dci.hobbyhubapi.search.domain;

import lombok.Data;

@Data
public class FiltersAttributes {
    private  String filName;
    private Integer filRating;
    private String filUbication;
    private Integer filMoneyFor;
    private Integer filMoneyUp;
}

