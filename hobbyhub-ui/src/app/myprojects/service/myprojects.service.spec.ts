import { TestBed } from '@angular/core/testing';

import { MyprojectsService } from './myprojects.service';

describe('MyprojectsService', () => {
  let service: MyprojectsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MyprojectsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
