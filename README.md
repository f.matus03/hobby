# HobbyHub-App
Aplicación HobbyHub para gestionar cursos utilizando Angular 17 y Spring Boot
### Resumen
HobbyHub-App es una aplicación diseñada para gestionar cursos. La aplicación se compone de dos componentes:
* Un componente UI construido con Angular 17, responsable de manejar las interacciones del usuario y mostrar datos.
* Un componente API construido con Spring Boot, responsable de procesar solicitudes y devolver respuestas.

## Arquitectura
La aplicación se compone de los siguientes componentes:

### UI (Angular 17)
El componente Angular 17 maneja las interacciones del usuario y comunica con la API para recuperar o modificar datos.

### API (Spring Boot)
El componente API de Spring Boot procesa solicitudes desde la UI y devuelve respuestas en formato JSON.

## Pruebas
Para probar la API, puede utilizar Insomnia o Postman importando el directorio `hobbyhub-api` (ubicado en la carpeta `test`) en su herramienta preferida de pruebas.

