import { Routes } from '@angular/router';
import {CourseListComponent} from "./search/components/courseList/courseList.component";
import {SearchComponent} from "./search/components/search/search.component";
import {HeaderComponent} from "./shared/components/header/header.component";


export const routes: Routes = [
  {path: '', component: HeaderComponent  },
  {path: 'search/searched', component: CourseListComponent},
  {path: 'search/search', component: SearchComponent}
];
