package cl.ufro.dci.hobbyhubapi.search.controller;

import cl.ufro.dci.hobbyhubapi.search.domain.Course;
import cl.ufro.dci.hobbyhubapi.search.domain.FiltersAttributes;
import cl.ufro.dci.hobbyhubapi.search.domain.Review;
import cl.ufro.dci.hobbyhubapi.search.service.SearchService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200/" )
@RestController
public class SearchController {
    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @PostMapping(value = "/search")
    public Iterable<Course> searchCourses(@RequestBody FiltersAttributes filtersAttributes) {
        return  searchService.searchCourses(filtersAttributes);
    }

    @GetMapping(value = "/course")
    public Iterable<Course>getCourses(){
        return searchService.getCourses();
    }
    @PostMapping(value = "/course")
    public  boolean addCourse(@RequestBody Course course){
        return searchService.addCourse(course);
    }

    @PostMapping(value = "/courses")
    public boolean addManyCourses(@RequestBody List<Course> listCourses){
        return searchService.addManyCourses(listCourses);
    }

    @PostMapping(value = "review/{id}")
    public boolean addReview(@PathVariable Long id, @RequestBody List<Review> reviews){
        return searchService.addReview(id,reviews );

    }
    @GetMapping(value = "/ubications")
    public List<String> getUbications(){
        return searchService.getUbications();
    }

}
