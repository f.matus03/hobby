package cl.ufro.dci.hobbyhubapi.search.repository;

import cl.ufro.dci.hobbyhubapi.search.domain.Course;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CoursesRepository extends CrudRepository<Course, Long> {

        @Query("SELECT c FROM Course c WHERE " +
            "(:name IS NULL OR c.couName LIKE CONCAT('%',:name,'%')) AND " +
            "(:ubication IS NULL OR c.couUbication = :ubication) AND " +
            "(:moneyFor IS NULL OR c.couPrice >= :moneyFor) AND " +
            "(:moneyUP IS NULL OR c.couPrice <= :moneyUP) AND " +
            "(:rating IS NULL OR (SELECT CAST(AVG(r.revRating) AS INTEGER ) FROM Review r WHERE r.revCourse = c) = :rating)")
        List<Course> findCoursesByFilters(@Param("name") String name,
                                      @Param("ubication") String ubication,
                                      @Param("moneyFor") Integer moneyFor,
                                      @Param("moneyUP") Integer moneyUP,
                                      @Param("rating") Integer rating);
                              
        @Query("SELECT DISTINCT c.couUbication FROM Course c ")
        List<String> findUbications();
}
