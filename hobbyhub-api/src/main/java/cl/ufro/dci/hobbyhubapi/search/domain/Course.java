package cl.ufro.dci.hobbyhubapi.search.domain;

import java.util.ArrayList;
import java.util.List;



import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;


@Data
@Entity
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long couId;

    private String couName;
    private String couUbication;
    private Integer couPrice;
    
    
    @OneToMany (mappedBy = "revCourse",cascade = CascadeType.ALL)
    private List <Review> couReviews = new ArrayList<>();


    public void addReview(Review review){
        this.couReviews.add(review);
    }
}
