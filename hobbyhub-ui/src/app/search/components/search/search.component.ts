import {Component, OnInit} from '@angular/core';
import {SearchService} from "../../service/search.service";
import {filters} from "../../domain/filters";
import {CommonModule} from "@angular/common";

@Component({
  selector: 'app-search',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './search.component.html',
  styleUrl: './search.component.css'
})
export class SearchComponent implements OnInit{
  courses:any;
  filters:filters = {};
  ubications:any;
  constructor(private courseService: SearchService) {
  }

  ngOnInit(): void{
    this.ubications = this.courseService.ObtainListUbications().subscribe(data =>{
      this.ubications = data;
    });
  }
  //La idea de esto es almacenar los cursos en el servicio y que la pagina /search/searched use los cursos almacenados en el servicio
  //por el momento no se a conseguido. De igual modo puede cambiar a lo que se almacene sean los filtros, es la misma idea. Por esto
  //mismo se tuvo que reescribir este buscador en el componente courseList. Ya que no se consiguio la comunicacion entre componentes del
  //modo que se queria

  public CollectData(name?: string, rating?: string, ubication?: string,
                           MoneyFor?: string, MoneyUp?: string) {

    this.FillFilters(name, rating, ubication, MoneyFor, MoneyUp);
    let check:boolean = this.Validators(name, MoneyFor, MoneyUp);
    if (check){
      this.ListCourses();
      window.location.href = "http://localhost:4200/search/searched";
    }
  }
  //Este metodo llena la variable filters
  private FillFilters(name?: string, rating?: string, ubication?: string,
                      MoneyFor?: string, MoneyUp?: string) {
    this.filters = {
      filName: name || null,
      filRating: Number(rating) || null,
      filUbication: ubication || null,
      filMoneyFor: Number(MoneyFor) || null,
      filMoneyUp: Number(MoneyUp) || null
    };
  }
  //Este es un validador que ve si cumple o no la condiciones para pasar a buscar dentro de la api
  private Validators(name?: string, MoneyFor?: string, MoneyUp?: string):boolean{
    if (name ==""){
      alert('El campo nombre no puede estar vacío.');
      return false;
    }
    if (Number(MoneyFor) > Number(MoneyUp)) {
      alert('El campo dinero desde no puede ser mayor a dinero hasta');
      return false;
    }
    return true;
  }
  //Metodo que llama al servicio que hace la busqueda hacia la api, llena la lista de cursos con lo que le devuelva el servicio
  private ListCourses(){
    this.courseService.ObtainListCourses(this.filters).subscribe(data =>{
      this.courses = data;
    });
  }
}
