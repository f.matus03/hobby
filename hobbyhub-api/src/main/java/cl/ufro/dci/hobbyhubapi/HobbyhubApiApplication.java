package cl.ufro.dci.hobbyhubapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HobbyhubApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HobbyhubApiApplication.class, args);
	}

}
