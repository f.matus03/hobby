import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { HeaderComponent } from "./shared/components/header/header.component";
import { ContentComponent } from "./shared/components/content/content.component";
import { FooterComponent } from "./shared/components/footer/footer.component";
import {CourseListComponent} from "./search/components/courseList/courseList.component";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'hobbyhub-ui';
}
