import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {filters} from "../domain/filters";

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  courses: any = [];
  ubications:any =[];
  private cursosUrl = "http://localhost:8080/search";
  private ubicationsUrl = "http://localhost:8080/ubications";
  constructor(private httpClient: HttpClient) {
  }
  ObtainListCourses(filters: filters):Observable<filters>  {
    this.courses = this.httpClient.post<any>(`${this.cursosUrl}`, filters);
    return this.ReturnListCourses();
  }
  ReturnListCourses():Observable<any>{
    return this.courses;
  }
  ObtainListUbications(): Observable<any>{
    return this.httpClient.get<any>(`${this.ubicationsUrl}`);
  }
}
