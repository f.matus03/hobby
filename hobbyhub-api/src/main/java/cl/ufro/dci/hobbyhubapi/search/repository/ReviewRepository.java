package cl.ufro.dci.hobbyhubapi.search.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import cl.ufro.dci.hobbyhubapi.search.domain.Review;

@Repository
public interface ReviewRepository extends CrudRepository<Review, Long>{
    

}
