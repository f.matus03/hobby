package cl.ufro.dci.hobbyhubapi.search.service;

import cl.ufro.dci.hobbyhubapi.search.domain.Course;
import cl.ufro.dci.hobbyhubapi.search.domain.FiltersAttributes;
import cl.ufro.dci.hobbyhubapi.search.domain.Review;
import cl.ufro.dci.hobbyhubapi.search.repository.CoursesRepository;
import cl.ufro.dci.hobbyhubapi.search.repository.ReviewRepository;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class SearchService {
    private final CoursesRepository coursesRepository;
    private final ReviewRepository reviewRepository;
    public SearchService(CoursesRepository coursesRepository, ReviewRepository reviewRepository) {
        this.coursesRepository = coursesRepository;
        this.reviewRepository = reviewRepository;
    }

    public boolean addCourse(Course course){
        try {
            coursesRepository.save(course);
            return  true;
        }catch (Exception e){
            return false;
        }
    }
    public Iterable<Course> getCourses(){
        return coursesRepository.findAll();
    }

    public boolean addManyCourses(List<Course> coursesList) {
        try {
            for (Course course : coursesList) {
                coursesRepository.save(course);
            }
            return true;
        } catch (Exception e) {
             return false;
        }
    }
    
    public Iterable<Course> searchCourses(FiltersAttributes filtersAttributes){
        String name  = filtersAttributes.getFilName();
        String ubication = filtersAttributes.getFilUbication();
        Integer moneyFor = filtersAttributes.getFilMoneyFor();
        Integer moneyUp = filtersAttributes.getFilMoneyUp();
        Integer rating = filtersAttributes.getFilRating();
        return  coursesRepository.findCoursesByFilters(name,ubication, moneyFor,moneyUp,rating);
    }

    public boolean addReview(Long id, List<Review> reviews) {
        try {
            Optional<Course> courseOptional = coursesRepository.findById(id);
            if (courseOptional.isPresent()) {

                for(Review review : reviews ){
                    Course course = courseOptional.get();
                    review.setRevCourse(course);
                    reviewRepository.save(review);
                    course.addReview(review);
                    coursesRepository.save(course);
                }
                return true; 
            
            } else {
                return false;
            }


        } catch (Exception e) {
            return false;
        }
    }
    
   public List<String> getUbications() {
    return coursesRepository.findUbications();
    }
}
