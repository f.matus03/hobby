import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RewiewsComponent } from './rewiews.component';

describe('RewiewsComponent', () => {
  let component: RewiewsComponent;
  let fixture: ComponentFixture<RewiewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RewiewsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RewiewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
