export interface Course {
  couId: number;
  couName: string;
  couUbication: string;
  couPrice: number;
  couAvgRating: number;
  couReviews:number;
}
