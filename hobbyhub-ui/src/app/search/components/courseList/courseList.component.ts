import {Component, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card'
import {SearchService} from "../../service/search.service";
import {filters} from "../../domain/filters";
import {MatPaginator, PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-course-c',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatButtonModule, MatPaginator],
  templateUrl: './courseList.component.html',
  styleUrl: './courseList.component.css'
})

export class CourseListComponent implements OnInit {
  courses:any;
  filters:filters = {};
  pageSlice: any;
  ubications:any = ["TEMUCO", "FBSFDBBBDDBLD"];
  constructor(private courseService: SearchService) {}

  //Cuando inicie el componente lo que este dentro de este ngOnInit se hara
  ngOnInit(): void{
  }
  //Este es el metodo el cual llama el boton search. Es el que contiene todos los parametros necesarias y llama a los demas
  //con el objetivo de llenar la lista
  public ObtainListCourses(name?: string, rating?: string, ubication?: string,
                           MoneyFor?: string, MoneyUp?: string) {

    this.FillFilters(name, rating, ubication, MoneyFor, MoneyUp);
    let check:boolean = this.Validators(name, MoneyFor, MoneyUp);
    if (check){
      this.ListCourses();
    }
    console.log(this.courses);
  }
  //Este metodo llena la variable filters
  private FillFilters(name?: string, rating?: string, ubication?: string,
  MoneyFor?: string, MoneyUp?: string) {
  this.filters = {
    filName: name || null,
    filRating: Number(rating) || 0,
    filUbication: ubication || null,
    filMoneyFor: Number(MoneyFor) || null,
    filMoneyUp: Number(MoneyUp) || null
  };
  }
  //Este es un validador que ve si cumple o no la condiciones para pasar a buscar dentro de la api
  private Validators(name?: string, MoneyFor?: string, MoneyUp?: string):boolean{
    if (name ==""){
      alert('El campo nombre no puede estar vacío.');
      return false;
    }
    if (Number(MoneyFor) > Number(MoneyUp)) {
      alert('El campo dinero desde no puede ser mayor a dinero hasta');
      return false;
    }
    return true;
  }
  //Metodo que llama al servicio que hace la busqueda hacia la api, llena la lista de cursos con lo que le devuelva el servicio
  private ListCourses(){
    this.courseService.ObtainListCourses(this.filters).subscribe(data =>{
      this.courses = data;
      this.pageSlice = this.courses.slice(0, 9);
    });
  }
  //Se encarga de variar los cursos para que haya un maximo de cursos en pantalla
  public OnPageChange(event: PageEvent) {
    const startIndex = event.pageIndex * event.pageSize;
    let endIndex = startIndex + event.pageSize;

    if(endIndex > this.courses.length){
      endIndex = this.courses.length;
    }
    this.pageSlice = this.courses.slice(startIndex, endIndex);
  }
}
