import { TestBed } from '@angular/core/testing';

import { BidderspanelService } from './bidderspanel.service';

describe('BidderspanelService', () => {
  let service: BidderspanelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BidderspanelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
